(function () {
    'use strict';
    angular
            .module('AsterGeek.App')
            .config(routeConfig);
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/login');
        
        $stateProvider
                .state('login', {
                    url: '',
                    abstract: true,
                    controller: 'MainController as vm',
                    views: {
                        'maincontainer': {
                            templateUrl: 'views/shared/login/index.html'

                        }
                    }
                });
        
        $stateProvider
                .state('login.Index', {
                    url: '/login',
                    controller: 'LoginController as vm',
                    templateUrl: 'views/shared/login.html'
//                    views: {
//                        "maincontainer@login": {                            
//                            templateUrl: 'views/shared/login.html'
//                        }
//                    }


                });
                
        $stateProvider
                .state('dashboard', {
                    url: '',
                    abstract: true,
//                    controller: 'DashboardController as vm',
                    views: {
                        'maincontainer': {
                            templateUrl: 'views/shared/dashboard/master.html',
                            controller: 'DashboardController as vm'
                        }
                    }
                });
                
//                 $stateProvider
//                .state('dashboard', {
//                    url: '',
//                    abstract: true,
////                    controller: 'DashboardController as vm',
//                    views: {
//                        'maincontainer': {
//                            templateUrl: 'views/shared/dashboard/master.html',
//                            controller: 'DashboardController as vm'
//                        }
//                    }
//                });
                
        $stateProvider
                .state('dashboard.crm', {
                    abstract: true,                    
                    views: {
                        "contentArea@dashboard": {
                            templateUrl: 'views/CRM/master.html',
                            controller: 'DashboardController as vm'
                        }
                    }
});

$stateProvider
                .state('dashboard.crm.index', {
                    url: '/crm',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    views: {
                        "contentArea@dashboard": {
                            templateUrl: 'views/CRM/Index.html'
                        }
                    }
});

$stateProvider
                .state('dashboard.crm.add', {
                    url: '/crm/add',                    
                    views: {
                        "crmContent@dashboard.crm": {
                            templateUrl: 'views/CRM/ContactForm.html',
                            controller: 'AddContactController',
                            controllerAs: 'vm'
                        }
                    }
});

        $stateProvider
                .state('root.index', {
                    url: '/'

                });
        

        $locationProvider.html5Mode(true);
//        $httpProvider.interceptors.push('RequestInterceptorService');
//        $httpProvider.interceptors.push('PostInterceptorService');
     //   $httpProvider.interceptors.push('RequestInterceptorService');
    }

})();



