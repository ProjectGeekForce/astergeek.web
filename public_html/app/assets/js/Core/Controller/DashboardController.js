(function () {
    'use strict';
    angular
            .module('AsterGeek.App')
            .controller('DashboardController', DashBoardController);
    DashBoardController.$inject = ['$scope', '$rootScope', 'AuthService'];
    
    function DashBoardController($scope, $rootScope, AuthService) {
        $scope.UserInfo = {
//          FullName:   AuthService.userAcctInfo.FullName,
//          FirstName:  AuthService.userAcctInfo.FirstName,
//          LastName:   AuthService.userAcctInfo.LastName,
//          EMail:      AuthService.userAcctInfo.EMail
        };
        
       
        $rootScope.$on('UserAcctInfoChange', function(){
            $scope.UserAccount.FullName = AuthService.UserAcctInfo;
        }); 
        
    }
    
})();
