(function () {
    'use strict';
    angular
            .module('AsterGeek.App')
            .controller('LoginController', LoginController);
    LoginController.$inject = ['$scope', 'UserAuth', '$timeout', '$state', '$rootScope', 'AuthService'];

    function LoginController($scope, UserAuth, $timeout, $state, $rootScope, AuthService) {


        $scope.Credentials = {
            Username: '',
            Passwd: '',
            RememberMe: false
        };

        $scope.Show = {
            LoginPrompt: true,
            LoggedInUserInformation: false
        };

        $scope.UserInfo = {
            FullName: '',
            FirstName: '',
            LastName: '',
            EMail: ''

        };
        $scope.Davis = "Davis Again";
        
        $scope.CheckLogin = checkLogin;

        $scope.Logout = logout;

        function checkLogin(credentials) {
            UserAuth.ValidateLogin(credentials)
                    .then(function (response) {
//                        console.log(JSON.stringify(response, null, 4));
                        switch (response.status) {
                            case 401:
                                FailedLogin(response);
                                break;
                            case 200:
                                SuccessfulLogin(response);
                                break;
                            default:
                                FailedLogin(response);
                                break;
                        }
                    });
        }

        function SuccessfulLogin(response) {
            var data = response.data;

            SetUserInfo(data);
            RedirectToDashboard();

        }

        function FailedLogin(response) {

            ShowErrorMessage();
        }
        function ToggleValue(prop, targetItem) {
            if (prop[targetItem] === true) {
                prop[targetItem] = false;
            } else {
                prop[targetItem] = true;
            }

        }

        function RedirectToDashboard() {
            $state.go('dashboard.Index');
        }
        function SetUserInfo(data) {
//            console.log('DATA: ' + JSON.stringify(data, null, 4));
//            console.log(data.AccountInfo.EMail);
            $scope.UserInfo.FullName = data.FullName;
            $scope.UserInfo.EMail = data.AccountInfo.EMail;
            $scope.UserInfo.FirstName = data.FullName.split(' ')[0];
            $scope.UserInfo.LastName = data.FullName.split(' ')[1];
            AuthService.SetUserAcctInfo($scope.UserInfo);
        }

        function logout() {
            $scope.Show.LoginPrompt = true;
            $scope.Show.LoggedInUserInformation = false;
        }

        function ShowErrorMessage() {
            $('.loginErrorMessage').css("opacity", 1);

            var timeout = $timeout(function () {
                $('.loginErrorMessage').css("opacity", 0);
            }, 10000);
        }

    }

})();

