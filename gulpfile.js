var gulp = require('gulp');
var gulpConfig = require('./public_html/src/assets/js/gulpConfig');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
// var minicss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var webserver = require('gulp-webserver');
var inject = require('gulp-inject');

var baseDir = 'public_html/app/assets/js/';
var helpers = baseDir + '*Helpers*/*.js';
var models = baseDir + '**/*Model*/*.js';
var modules = baseDir + '**/app.js';
var routes = baseDir + '**/routes.config.js'; 
var controllers = baseDir + '**/*Controller.js';
var directives = baseDir + '**/Directive*/*.js';
var filters = baseDir + '**/Filter*/*.js';
var services = baseDir + '**/Service*/*.js';

var allAngularCustomFiles = [helpers, models, modules, routes, controllers, directives, filters, services];

gulp.task('watch', function () {
    console.log(gulpConfig.styleConfig.SRC_ROOTDIR + '**/*.*');
    gulp.watch([gulpConfig.appConfig.APP_ROOTDIR  + '**/*.*' , gulpConfig.styleConfig.SRC_ROOTDIR + '**/*.*'], ['less']);
});

gulp.task('less', function () {
    return gulp.src(gulpConfig.styleConfig.LESS_FILES_ALL)
            .pipe(sourcemaps.init())
            .pipe(less({}))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(sourcemaps.write('../maps'))
            .pipe(gulp.dest(gulpConfig.styleConfig.CSS_APPDIR))
            .pipe(gulp.dest(gulpConfig.styleConfig.CSS_SRCDIR));
});


gulp.task('customAngularJS', function () {
    var sources = gulp.src(allAngularCustomFiles, {read: false});
    var target = gulp.src(gulpConfig.appConfig.INDEX_FILE_APPDIR);
    
    return target.pipe(inject(sources, {name: 'angular', relative: true}))
            .pipe(gulp.dest(gulpConfig.appConfig.APP_ROOTDIR));


});

gulp.task('CopySrcCSS', function () {
    gulp.src('src/assets/css/**/*.css')
            .pipe(gulp.dest('app/assets/css/'));
});

gulp.task('InstallAngularRequirement', function () {
    
    var bowerRoot = gulpConfig.appConfig.APP_ROOTDIR + 'assets/bower_components/';
    var jQuerySrc = bowerRoot + 'jquery/dist/jquery.min.js';
    var angular = bowerRoot + 'angular/angular.min.js';
    var uiRouter = bowerRoot + 'angular-ui-router/release/angular-ui-router.js';

    var fontAwesome = bowerRoot + 'font-awesome/css/font-awesome.min.css';
    var slyFramework = bowerRoot + 'SlyFramework/dist/slyFramework.min.css';

    var customCSS = gulpConfig.styleConfig.CSS_APPDIR + '/custom.css';
    var AllOtherCSS = gulpConfig.styleConfig.CSS_APPDIR + '/**.css, !' + gulpConfig.styleConfig.CSS_APPDIR + '/custom.css';

    var srcIndex = gulpConfig.appConfig.INDEX_FILE_APPDIR;

    var target = gulp.src(srcIndex);
    var sources = gulp.src([jQuerySrc, angular, uiRouter, fontAwesome, slyFramework, customCSS, AllOtherCSS], {read: false});

    return target.pipe(inject(sources, {relative: true}))
            .pipe(gulp.dest(gulpConfig.appConfig.APP_ROOTDIR));


});

gulp.task('webServerDev', function () {
    gulp.src(['./public_html/app/'])
            .pipe(webserver({
                livereload: {
                    enable: true
                },
                path: '/',
                directoryListing: false,
                open: true,
                host: 'localhost',
                https: false,
                port: 5606,
                fallback: 'index.html'
//                middleware: 
            }));
});



gulp.task('default', ['webServerDev', 'watch', 'less', 'InstallAngularRequirement', 'customAngularJS', 'CopySrcCSS']);

