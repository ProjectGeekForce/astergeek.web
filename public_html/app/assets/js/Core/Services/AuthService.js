(function () {
    'use strict';
    
    angular
            .module('AsterGeek.App')
            .factory('AuthService', Auth);
    
    Auth.$inject = ['$rootScope'];
    
   
    function Auth($rootScope) {
        var userAcctInfo = {};
        
        var factory = {};
        
        
        factory.UserAcctInfo = this.userAcctInfo;
        
        factory.SetUserAcctInfo = function(userInfo){
//            console.log('UserInfo: ' + JSON.stringify(userInfo, null, 4));
            this.userAcctInfo = userInfo;
            SetUserAcctInfoToSessionStorage(userInfo);
            $rootScope.$broadcast('UserAcctInfoChange', { UserInfo: factory.UserAcctInfo});
        };
        
        factory.Logout = function(){
            this.userAcctInfo = {};
            ClearSessionInfo();
            $rootScope.$broadcast('UserAcctInfoChange', {});
            
        };
        
        factory.GetFullName = function(name){
            return GetSession(name);
        };
        
        function ClearSession(name){
            sessionStorage.removeItem(name);
        }
        
        function GetSession(name){
            return sessionStorage.getItem(name);
        }
        
        function SetSession(name, value){
            sessionStorage.setItem(name, value);
        }
        
        function ClearUserInfo(){
            ClearSession('Email');
            ClearSession('FullName');
            ClearSession('FirstName');
            ClearSession('LastName');
        }
        
        function SetUserAcctInfoToSessionStorage(data){
            SetSession('Email', data.EMail);
            SetSession('FullName', data.FullName);
            SetSession('FirstName', data.FirstName);
            SetSession('LastName', data.LastName);
        }
        return factory;
    }
})();


