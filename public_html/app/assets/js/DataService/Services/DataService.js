(function () {
    'use strict';

    angular
            .module('AsterGeek.App.DataService')
            .factory('DataService', DataService);

    DataService.$inject = ['$http', 'DeploymentConfigService'];

    function DataService($http, DeploymentConfigService) {

        var BaseURL = DeploymentConfigService.BaseURL;  // http://localhost:6902/api

        var factory = {};

        factory.Post = function (apiPath, data, config) {
            return $http.post(BaseURL + apiPath, data, config);
//                    .then(
//                            function (response) {
//                                return response;
//                            },
//                            function (response) {
//                                return response;
//                            }
//                    );;

        };

        factory.Get = function (apiPath, config) {
            $http.get(apiPath, config)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (response) {
                                return response;
                            }
                    );
        };

        factory.Put = function (apiPath, data, config) {
            $http.put(apiPath, data, config)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (response) {
                                return response;
                            }
                    )
        };

        factory.Delete = function (apiPath, data, config) {
            $http.delete(apiPath, config)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (response) {
                                return response;
                            }
                    )
        };

        return factory;
    }
})();


