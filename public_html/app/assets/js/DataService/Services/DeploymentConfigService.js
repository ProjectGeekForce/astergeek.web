(function () {
    'use strict';
    
    angular
            .module('AsterGeek.App.DataService')
            .factory('DeploymentConfigService', DeploymentService);
    
    DeploymentService.$inject = [];
    
    function DeploymentService() {
        var ProdDeploy = false;
        
        var DEV_URL = 'http://localhost:6903/API'; // http://localhost:6903/api';
        //var DEV_URL = 'http://localhost:6903/api'; // http://localhost:6903/api';
        var PROD_URL = 'http://192.168.0.6:6903/api';
        
        var BASEURL = (ProdDeploy) ? PROD_URL : DEV_URL;
        
        var factory = {};
        
        factory.ProductionDeployment = ProdDeploy;
        factory.BaseURL = BASEURL;
        
        return factory;
    }
})();


