(function () {
    'use strict';
      
 angular
            .module('AsterGeek.App')
            .factory('RequestInterceptorService', RequestInterceptorService);
    
    RequestInterceptorService.$inject = ['$q'];

    function RequestInterceptorService($q){

    var startTime = 0;
    var endTime = 0;
    var difference = 0;
    
    
    var factory = {
        
        request: function (config) {
            if (config.url.indexOf('/UserAuth') > 0) {
                
//                console.log(new Date());
//                console.log('This is my Interceptor Data config- REQUEST: ' + JSON.stringify(config, null, 4));
                startTime = new Date();
                $('.loadingIcon').css('opacity', "1");
            }
            return config;
        },
        response: function (config) {  
            
            if (config.config.url.indexOf('/UserAuth') > 0) {
                $('.loadingIcon').css('opacity', "0");
                endTime = new Date();
                difference = (endTime - startTime) / 1000;
                
               // console.log('The Request for ' + config.config.url + ' took ' + difference + ' seconds to complete');
            }
            return config;
        },
        
        responseError: function(config){
             if (config.config.url.indexOf('/UserAuth') > 0){
            $('.loadingIcon').css('opacity', "0");
             };
            return config;
        },        
        requestError: function(config){
            if (config.config.url.indexOf('/UserAuth') > 0){
            $('.loadingIcon').css('opacity', "0");
        }
            return config;
        }
        

    };
    return factory;
    }
})();

