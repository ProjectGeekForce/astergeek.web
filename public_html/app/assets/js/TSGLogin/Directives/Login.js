(function () {
    'use strict';

    angular
            .module('TSG.Login', [])
            .directive('login', LoginDirective);

    LoginDirective.$inject = ['$timeout', 'UserAuth'];

    function LoginDirective($timeout, UserAuth) {
        return {
            restrict: "EA",
            link: function (scope, element, attrs) {



            },
//            templateUrl: './index.html',
            templateUrl: 'assets/js/TSGLogin/Directives/index.html',
            controller: function ($scope) {
                
                $scope.Credentials = {
                    Username: '',
                    Passwd: '',
                    RememberMe: false
                };
                
                $scope.Show = {
                    LoginPrompt: true,
                    LoggedInUserInformation: false
                };
                
                $scope.UserInfo = {
                    FullName: '',
                    FirstName: '',
                    LastName: '',
                    EMail: ''

                };

                $scope.CheckLogin = checkLogin;

                $scope.Logout = logout;

                function checkLogin(credentials) {
                    UserAuth.ValidateLogin(credentials)
                            .then(function (response) {

                                console.log(JSON.stringify(response, null, 4));
                                var data = response.data;

                                ToggleValue($scope.Show, 'LoginPrompt');
                                ToggleValue($scope.Show, 'LoggedInUserInformation');

                                SetUserInfo(data);

                            },
                                    function (response) {
                                        ShowErrorMessage();
                                        
                                    });

                }

                function ToggleValue(prop, targetItem) {
                    if (prop[targetItem] === true) {
                        prop[targetItem] = false;
                    } else {
                        prop[targetItem] = true;
                    }

                }

                function SetUserInfo(data) {
                    $scope.UserInfo.FullName = data.FullName;
                    $scope.UserInfo.EMail = data.AccountInfo.Email;
                }

                function logout() {
                    $scope.Show.LoginPrompt = true;
                    $scope.Show.LoggedInUserInformation = false;
                }
                
                function ShowErrorMessage() {
                    $('.loginErrorMessage').css("opacity", 1);
                    
                    var timeout = $timeout(function(){ 
                        $('.loginErrorMessage').css("opacity", 0);
                    }, 10000);
                }

                
                
                
            }
        };
    }
})();


