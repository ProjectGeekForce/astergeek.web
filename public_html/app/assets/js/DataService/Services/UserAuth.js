(function () {
    'use strict';
    
    angular
            .module('AsterGeek.App.DataService')
            .factory('UserAuth', UserAuth);
    
    UserAuth.$inject = ['DataService'];
    
    function UserAuth(DataService) {
        
        var UserAuthAPI = '/UserAuth'; // http://localhost:6902/apihttp://localhost:6902/api/UserAuth
        var config = { 
        
        };
        var factory = {};
        
        factory.ValidateLogin = function(credentials){
            
           
            return DataService.Post(UserAuthAPI, credentials, config);
//                    .then(function(response){
//                        console.log('User Auth - Success: ' + JSON.stringify(response, null, 4));
//                return response;
//                    },
//                            function(response){
//                              console.log('User Auth - Failure: ' + JSON.stringify(response, null, 4));
//                            });            

        };
        
        
        
        return factory;
    }
})();


