var siteRoot = 'public_html/';
var appRoot = siteRoot + 'app/';
var srcRoot = siteRoot + 'src/';
var CSS_SRCDIR =srcRoot + 'assets/css';
var CSS_APPDIR =   appRoot + 'assets/css';
var LESS_SRCDIR =  srcRoot + 'assets/less';
var CSS_MAPDIR =  '../map';
var LESS_FILES_SRCDIR =  LESS_SRCDIR + '/*.less';
var CSS_FILES_APPDIR =  CSS_APPDIR + '/*.css';
var LESS_FILES_ALL = LESS_SRCDIR + '/**/*.less';
var SRC_ROOTDIR =  srcRoot;

var gulpConfig = {
    CSS_SRCDIR:  CSS_SRCDIR,
    CSS_APPDIR:  CSS_APPDIR,
    LESS_SRCDIR: LESS_SRCDIR,
    CSS_MAPDIR: CSS_MAPDIR,
    LESS_FILES_SRCDIR: LESS_FILES_SRCDIR,
    CSS_FILES_APPDIR: CSS_FILES_APPDIR,
    LESS_FILES_ALL: LESS_FILES_ALL,
    SRC_ROOTDIR: SRC_ROOTDIR
};

var appConfig = {
    INDEX_FILE_APPDIR:  appRoot + '/index.html',
    APP_ROOTDIR:  appRoot
};



module.exports = {
    styleConfig: gulpConfig,
    appConfig: appConfig

};