(function () {
    'use strict';
    
    angular
            .module('TSG.GoogleAPI')
            .factory('GeoLocation', GeoLocation);
    
    GeoLocation.$inject = ['DataService'];
    
    function GeoLocation(DataService) {
        
        var factory = {};
        
        var apiKey = 'AIzaSyDO9mdozyuZRLYhou9Dbv2fGqMu68Jne7w';
        var BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?key=' + apiKey;
        
        factory.ReverseGeolocation = function (latitude, longitude) {
            var apiURL = '&latlng=' + latitude + ',' + longitude;
            var URL = BASE_URL + apiURL;
            
            DataService.Post(URL);
        };
        
        return factory;
    }
})();


